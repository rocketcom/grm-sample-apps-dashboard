# GRM Sample Apps: Dashboard

## Description
Given the large number of satellite contacts and equipment assets that operators are responsible for, maintaining situational awareness poses a significant challenge. Operators must be able to quickly identify equipment issues and resolve them so that there are no missed opportunities to communicate with satellites. The GRM Dashboard app was designed with this goal in mind. As the operators’ primary GRM app, it would constantly occupy one of their large displays.

There are three main areas of the GRM Dashboard app: the Global Status Bar, the Alerts pane and a tabbed content area that displays either the Contacts page or the Equipment page.

## Prerequisites
1. A UNIX-like OS (Linux, Mac OS, FreeBSD, etc.)*
2. [Node.js](https://nodejs.org/)
3. [npm](https://www.npmjs.com/get-npm)
4. [Polymer](https://www.polymer-project.org/)
5. [EGS Web Socket Server](https://bitbucket.org/rocketcom/egs-socket-server/src/master/) (optional)**
6. [EGS Web Services Server](https://bitbucket.org/rocketcom/egs-data-services/src/master/) (optional)**

\* It may be possible to run on Windows but has not been thoroughly tested and is not recommended.
\*\* You only need to run your own Web Socket and Web Services servers if you are behind a firewall that prevents you from accessing the ones we provide at <wss://sockets.astrouxds.com> and <https://services.astrouxds.com> respectively.


## Getting Started
### Clone this repository

`git clone git@bitbucket.org:rocketcom/grm-sample-apps-dashboard.git`

### Install the Polymer CLI

`npm i -g polymer-cli`

### Install NPM modules for this project

`npm i`

### Create a symbolic link to the appropriate file in the `config` directory.

We recommend linking to config.local.json:

`ln -s config.local.json config.json`

However, if you are running your own Web Socket and Web Services servers (see above) you will need to create your own config file that points to your own servers. Use one of the existing config files as a template and substitute the appropriate URLs.

### For local development and testing

`npm run start`

### To build for production

`npm run build`

### Load the application(s) in your browser

Assuming you've linked your config file to config.local.json:

<http://localhost:8000>

Otherwise, see values from your config file and/or the output from `npm run start`