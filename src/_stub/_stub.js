import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/*

  To make use of external HTML templates and CSS import them
  via standard ES6 import statements.

  Note: This is not a true import Webpack will copy the
  content of each file when it generates its build bundle

*/
import template from './_stub.html';
import css from './_stub.css';

/**
 * @polymer
 * @extends HTMLElement
 */
export class StubComponent extends PolymerElement {
  // Properties in HTML should be hyphenated, when referenced
  // in Javascript use camelCase and remove the hyphen
  static get properties() {
    return {
      genericProperty: {
        type: String,
      },
    };
  }

  constructor() {
    super();
  }

  connectedCallback() {
    super.connectedCallback();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
  }

  ready() {
    super.ready();
  }

  static get template() {
    return html([
      ` <style include="astro-css">
          ${css}
        </style> 
        ${template}`,
    ]);
  }
}

customElements.define('stub-component', StubComponent);
